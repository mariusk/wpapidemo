This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

It demonstrates usage of the open Oliasoft Wellplan&trade; (WP) API from a
simple React-based web application (SPA/PWA).

To actually run the demo you need valid user credentials for WP and a
link to any designs created.

The most relevant source code can be viewed here:

https://gitlab.com/mariusk/wpapidemo/blob/master/src/App.js

Here's an image of the demo running live (to the left), with WP to the right:

![demo image](wpapidemo.png)
