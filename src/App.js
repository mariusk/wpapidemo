import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const REFRESH_SECONDS = 5;

function nn(v) { return isNaN(v) ? '-' : v.toFixed(2); }

function wellSummaryTable(str, rows) {
  const s = {border: '1px solid #d0d0d0', padding: '2px 2px 2px 6px'};

  const rowEls = [];
  let ctr = 1;
  const highlightLcs = {};
  rows.forEach(([casing, stringsdata], casingix) => {
    stringsdata.forEach(([string, ...min_sf_cols], stringix) => {
      const cols = [];
      if (!stringix) {
        cols.push(casingix+1, casing.section, casing.name+' '+casing.type);
      }
      cols.push(string.od, string.weight, string.grade, string.conntype,
                string.top, string.bottom);
      min_sf_cols.forEach(min_sf => {
        const shortnameMatch = min_sf[3].match(/^\S+/);
        const shortname = shortnameMatch ? shortnameMatch[0] : '';
        if (shortname !== '-')
          highlightLcs[shortname] = 1;
        cols.push(min_sf[0] ? nn(min_sf[0]) : '', shortname);
      });
      const colEls = cols.map((col, colix) => {
        //const rowSpan = !colix ? stringsdata.length : 1;
        const rowSpan = (!stringix && (colix < 3)) ? stringsdata.length : 1;
        const align = (col+'').match(/[^0-9.-]/) ? 'left' : 'right';
        return (<td style={{textAlign: align, ...s}}
                className="App-trackChange"
                key={colix+col}
                    rowSpan={rowSpan}>{col}</td>);
      });
      rowEls.push(
        <tr key={ctr++}>{colEls}</tr>
      );
    });
  });

  const el = (
    <div>
      <div key={str} className="App-trackChange">{str}</div>
      <table style={{borderCollapse: 'collapse'}}>
        <thead>
          <tr>
            <th style={s} rowSpan="2">#</th>
            <th style={s} rowSpan="2">Section</th>
            <th style={s} rowSpan="2">Type</th>
            <th style={s} rowSpan="2">OD</th>
            <th style={s} rowSpan="2">Weight</th>
            <th style={s} rowSpan="2">Grade</th>
            <th style={s} rowSpan="2">Conn.type</th>
            <th style={s} rowSpan="2">Top</th>
            <th style={s} rowSpan="2">Bottom</th>
            <th style={s} colSpan="8">Safety Factors</th>
          </tr>
          <tr>
            <th style={s} colSpan="2">Burst</th>
            <th style={s} colSpan="2">Collapse</th>
            <th style={s} colSpan="2">Axial</th>
            <th style={s} colSpan="2">Triaxial</th>
          </tr>
        </thead>
        <tbody>
          {rowEls}
        </tbody>
      </table>
    </div>
  );

  return [el, highlightLcs];
};

const API_URL = 'https://app.oliasoft.com/api';
//const API_URL = 'http://localhost:8006/api';

class App extends Component {

  constructor() {
    super();
    ['onClickFetch', 'onClickManualCalc', 'onClickContinuousCalc']
      .map(s => this[s] = this[s].bind(this));
    this.state = {
      datasetdb: null,
      summary: null,
      token: null,
      auto: false
    };
  }

  refresh() {
    if (!this.state.auto)
      return;

    this.evalDataset();

    setTimeout(this.refresh.bind(this), REFRESH_SECONDS*1000);
  }

  login(email, password) {
    return fetch(API_URL+'/login', {
      method: 'POST',
      body: JSON.stringify({email, password}),
      headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
      cache: 'no-cache'
    })
      .then(res => res.json())
      .then(json => this.setState({token: json.token}))
      .catch(err => console.error(err));
  }

  evalDataset() {
    this.fetchDataset()
      .then(() => {
        return fetch(API_URL+'/evalds', {
          method: 'POST',
          headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
          cache: 'no-cache',
          body: JSON.stringify({
            t: this.state.token,
            dataset: this.state.datasetdb
          })
        })
          .then(res => res.json())
          .then(json => this.setState({ts: (new Date()).toString(),
                                       summary: json}))
          .catch(err => console.error(err));

      });
  }

  fetchDataset() {
    return fetch(API_URL+'/loadds', {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
      cache: 'no-cache',
      body: JSON.stringify({
        type: 'casing',
        t: this.state.token,
        datasetid: this.state.datasetid,
        groupid: this.state.groupid
      })
    })
      .then(res => res.json())
      .then(json => this.setState({datasetdb: json.datasetdb}))
      .catch(err => console.error(err));
  }

  onClickManualCalc() {
    console.log('onClickManualCalc', this.state.datasetdb);
    this.evalDataset();
  }

  onClickContinuousCalc() {
    console.log('onClickContinuousCalc', !this.state.auto);
    this.setState({auto: !this.state.auto}, () => this.refresh());
  }

  onClickFetch() {

    const fullurl = this.url_input.value;
    const email = this.email_input.value;
    const password = this.password_input.value;

    // Extract groupid and datasetid from url
    const match = fullurl.match(/^http.*\/app\/wellplan\/(\d+)\/[^/]+\/(\d+)/);
    if (!match)
      return;
    const groupid = match[1];
    const datasetid = match[2];

    this.setState({groupid, datasetid}, () => {
      return this.login(email, password)
        .then(() => this.fetchDataset());
    });

  }

  render() {
    const {datasetdb} = this.state;
    let summarytable;
    if (this.state.summary) {
      summarytable = wellSummaryTable(this.state.ts, this.state.summary.casing_rows);
    }

    return (
      <div className="App">

        <header>
          <img src={logo} className="App-logo" alt="logo" />
          Wellplan&trade;API Demo
        </header>

        <p>
          This app demonstrates simple usage of the Oliasoft Wellplan API for:
        </p>

        <ol>
          <li>Retrieving casing designs</li>
          <li>Trigger design calculations</li>
          <li>Continuous calculations and updates</li>
        </ol>

        <h1>1. Fetching Casing Designs</h1>
        <p>
          Email: <input type="text" size="20" ref={el => this.email_input = el} />
          &nbsp;Password: <input type="password" size="20" ref={el => this.password_input = el} />
        </p>
        <p>
          Casing design URL:
          &nbsp;<input type="text" size="60" ref={el => this.url_input = el}/>
        </p>
        <p>
          <button onClick={this.onClickFetch}>Fetch...</button>
        </p>

        <p>
          Casing Design: <b>{(datasetdb && datasetdb.name) || '(none)'}</b>
        </p>

        <h1>2. Triggering Calculations</h1>
        <button onClick={this.onClickManualCalc} disabled={!datasetdb}>Manual Recalc</button>

        <h1>3. Continuous Calculations and Updates</h1>
        <button onClick={this.onClickContinuousCalc} disabled={!datasetdb}>{this.state.auto ? 'Stop' : 'Start'}</button>

        <h1>Output / Result:</h1>
        <pre>{(summarytable && summarytable[0]) || '(nothing to show yet)'}</pre>
      </div>
    );
  }
}

export default App;
